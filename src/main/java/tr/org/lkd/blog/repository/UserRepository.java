package tr.org.lkd.blog.repository;

import org.springframework.data.repository.CrudRepository;
import tr.org.lkd.blog.model.User;

import java.util.Optional;

/**
 * Created on January, 2018
 *
 * @author adilcan
 */
public interface UserRepository extends CrudRepository<User, Integer> {

	public Optional<User> findByUsername(String username);

}
