package tr.org.lkd.blog.repository;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import tr.org.lkd.blog.model.Entry;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

/**
 * Created on January, 2018
 *
 * @author adilcan
 */
@Component
@Transactional
public interface EntryRepository extends CrudRepository<Entry, Integer> {

    @Query("SELECT e FROM Entry e")
    List<Entry> findAllEntries();

    @Query("SELECT e FROM Entry e WHERE id = :id")
    Entry findEntryById(@Param("id") Integer id);

    @Modifying
    @Query("DELETE FROM Entry WHERE id = :id")
    void deleteEntryById(@Param("id") Integer id);

    @Query("SELECT e FROM Entry e WHERE e.title LIKE %:title%")
    List<Entry> findByTitle(@Param("title") String title);

    List<Entry> findByCreateDate(LocalDate date);

}
