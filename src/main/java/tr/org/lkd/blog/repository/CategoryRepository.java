package tr.org.lkd.blog.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tr.org.lkd.blog.model.Category;

/**
 * Created on January, 2018
 *
 * @author adilcan
 */
@Repository
public interface CategoryRepository extends CrudRepository<Category, Integer> {

}
