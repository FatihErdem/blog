package tr.org.lkd.blog.model;

/**
 * Created on January, 2018
 *
 * @author adilcan
 */
public enum Role {

	USER, ADMIN;
}
